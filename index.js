require('dotenv').config();

const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);
const express = require('express')
const app = express()
/*
client.messages
  .create({
     body: 'Hola, este numero de Whatsapp solo es utilizado para notificaciones automáticas. Recuerda que si tienes cualquier duda nos puedes contactar al 3182725275 y con gusto te atenderemos!',
     from: 'whatsapp:+19382533464',
     to: 'whatsapp:+573132852046'
   })
  .then(message => console.log(message));
*/

const getRawBody = require('raw-body')
const crypto = require('crypto')
const secretKey = '86aa2013f29d033fe97d3a56e1fe579674b7e0d0937668cd886f1600c2b6837e'

app.post('/prod/apimaqui', async (req, res) => {
  console.log('🎉 We got an order!')

  // We'll compare the hmac to our own hash
  const hmac = req.get('X-Shopify-Hmac-Sha256')

  // Use raw-body to get the body (buffer)
  const body = await getRawBody(req)

  // Create a hash using the body and our key
  const hash = crypto
    .createHmac('sha256', secretKey)
    .update(body, 'utf8', 'hex')
    .digest('base64')

  // Compare our hash to Shopify's hash
  if (hash === hmac) {
    // It's a match! All good
    console.log('Phew, it came from Shopify!')
    res.sendStatus(200)
  } else {
    // No match! This request didn't originate from Shopify
    console.log('Danger! Not from Shopify!')
    res.sendStatus(403)
  }

const order = JSON.parse(body.toString())
console.log(order.customer)
console.log(order.customer.phone)
/*  */
if(order.customer.phone != null){
    seadMesagge(order.customer.first_name, order.customer.phone);
}
else if(order.customer.default_address.phone != null){
    seadMesagge(order.customer.first_name, order.customer.default_address.phone);
  }
})
function seadMesagge(name, phone){
  if(phone.indexOf("+57") == -1){
    phone = '+57'+phone;
  }
  console.log(phone);
  client.messages
  .create({
     body: '¡Hola '+name+', hemos recibido tu pedido y pago exitosamente! Te estaremos informando por este mismo medio el estado de tu pedido conforme vaya avanzando. ¡Muchas gracias por tu compra! https://www.maquiactive.com/',
     from: 'whatsapp:+18635910528',
     to: 'whatsapp:'+phone
   })
  .then(message => console.log(message));
}

app.listen(3000, () => console.log('Example app listening on port 3000!'))


/*

app.post('/webhooks/orders/create', (req, res) => {
  console.log(req)
  res.sendStatus(200)
});

app.listen(3000, () => console.log('Example app listening on port 3000!'))
*/